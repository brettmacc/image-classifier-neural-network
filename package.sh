#!/bin/bash

pip install numpy
pip install h5py
pip install Pillow
pip install scipy
pip install opencv-python
pip install requests
pip install matplotlib
pip install tensorflow
