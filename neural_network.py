'''
    Brett Mac: March 2019

	This is a "handwritten" neural network. All algorithms and loss functions are implemented.
	Not as effective as tensorflow/keras performance wise, just a learning tool for the math behind neural networks.

'''

import numpy as np
import matplotlib.pyplot as plt
import h5py
import scipy
from PIL import Image
from scipy import ndimage


# Hyper-parameters for the network. Can configure everything here
num_iterations = 101
learning_rate = 0.01
regular_rate = 0.7
drop_prob = 0.90
momentum = 0.9
hidden_layers = [10, 5, 7, 1]
activation_layers = ["relu", "relu", "relu", "sig"]


# This is the main function and starts the neural network
def start():
    # first step, get all image data
    train_x_orig, train_y, test_x_orig, test_y, classes = load_dataset()
    hidden_layers[len(hidden_layers)-1] = len(classes) # autoset last layer to correct dimension

    # reshape image data into a [Nx x m] matrix and normalize matrix
    train_x = (train_x_orig.reshape(train_x_orig.shape[0], -1).T) / 255.
    test_x = (test_x_orig.reshape(test_x_orig.shape[0], -1).T) / 255.

    # create machine learning model
    train_data = {"train_x": train_x, "train_y": train_y}
    params = nn_model(train_data)

    # get predictions from trained model
    predictions = prediction(test_x, params)

    # need original test_x data to print image
    #print_predictions(test_x_orig, test_y, predictions, classes)


# This loads the h5 dataset file (in the program directory)
def load_dataset():
    # train/test data files are separated
    train_dataset = h5py.File('./datasets/single_100p.h5', "r")

	# separate train/test data based on percentage
    train_x = np.array(train_dataset["train_images"][:])
    test_x  = np.array(train_dataset["test_images"][:])
    train_y = np.array(train_dataset["train_labels"][:]).T
    test_y  = np.array(train_dataset["test_labels"][:]).T

    # class names are available (from folder names in directory)
    classes = np.array(train_dataset["classes"][:])

    return train_x, train_y, test_x, test_y, classes


# create a machine learning nn model. initalize parameters and run optimization loop
def nn_model(train_data):
    # initialize (w, b) parameters based on input data, hidden layers, output nodes
    params = initialize_params(train_data["train_x"])

    # optimization loop
    last_cost = 0
    for i in range(num_iterations):
        # step 1) prop all examples forward through all layers
        cache = propogate_forward(train_data["train_x"], params)

        # step 2) calculate loss for all examples and get cost function
        cost = cost_function(train_data["train_y"], cache, params, i)

        if (i % 10 == 0):
            print("\nIteration " + str(i) + ", cost: " + str(cost))

        '''
        # check cost function correctness
        if (cost > last_cost) and (i > 0):
            print("\nInceased Cost Function = " + str(cost))
        last_cost = cost
        '''

        # step 3) prop backwards through layers to determine paramater adjustment
        gradients = propogate_backward(train_data["train_y"], cache, params)

        # step 4) update params based on back propogation
        params = update_params(params, gradients)

    return params


# initialize w and b vectors for all layers based on hidden_layers array and input data
def initialize_params(train_x):
    last_x, params = train_x.shape[0], {}

    # params object holds all w, b parameters (for all layers). W1 -> WL, b1 -> bL
    for i in range(len(hidden_layers)):
        params["W"+str(i+1)] = np.random.randn(hidden_layers[i], last_x) * np.sqrt(2 / last_x)
        params["b"+str(i+1)] = np.zeros((hidden_layers[i], 1))

        # add momentum parameters
        params["vW" + str(i+1)] = np.zeros((hidden_layers[i], last_x))
        params["vb" + str(i+1)] = np.zeros((hidden_layers[i], 1))
        last_x = hidden_layers[i]

    return params


# propogate forward through all layers. calculate linear combination and activation function
def propogate_forward(train_x, params):
    cache = {"A0": train_x}

    # cache holds all a, z params for linear/activation comination
    for i in range(len(hidden_layers)):
        cache["Z"+str(i+1)] = np.dot(params["W"+str(i+1)], cache["A"+str(i)]) + params["b"+str(i+1)]
        cache = activation(cache, i)

        # dropout regularization
        #if (i != len(hidden_layers) - 1):
            #cache["D"+str(i+1)] = np.random.rand(cache["A"+str(i+1)].shape[0], cache["A"+str(i+1)].shape[1])
            #cache["A"+str(i+1)] = cache["A"+str(i+1)] * (cache["D"+str(i+1)] < drop_prob) / drop_prob

    return cache


# based on hyperparameters, set activation/deactivation function for each layer
def activation(cache, i):
    if (activation_layers[i] == "sig"):
        cache["A"+str(i+1)] = sigmoid(cache["Z"+str(i+1)])
        cache["dA"+str(i+1)] = dsigmoid(cache["A"+str(i+1)])
    if (activation_layers[i] == "tanh"):
        cache["A"+str(i+1)] = tanh(cache["Z"+str(i+1)])
        cache["dA"+str(i+1)] = dtanh(cache["A"+str(i+1)])
    if (activation_layers[i] == "relu"):
        cache["A"+str(i+1)] = relu(cache["Z"+str(i+1)])
        cache["dA"+str(i+1)] = drelu(cache["A"+str(i+1)])
    return cache


# sigmoid activation function
def sigmoid(z):
    return 1 / (1 + np.exp(-z))
# dsigmoid derivative function
def dsigmoid(z):
    return z * (1 - z) 
# tanh activation function
def tanh(z):
    return np.tanh(z)
# tanh derivative function
def dtanh(z):
    return 1 - (z * z)
# relu activation function
def relu(z):
    return np.maximum(0, z)
# drelu derivative function
def drelu(z):
    return np.int64(z > 0)


# calculate the average sum of the loss
def cost_function(train_y, cache, params, index):
    # calculate logistic loss cost function
    AL = cache["A"+str(len(hidden_layers))]
    cost = -np.sum(np.multiply(np.log(AL), train_y) + np.multiply(np.log(1-AL), 1-train_y)) * (1. / train_y.shape[1])

    # compute regularization cost
    lambd = 0
    for i in range(len(hidden_layers)):
        lambd += np.sum(np.square(params["W"+str(i+1)]))
    regularization = lambd * regular_rate / (2 * train_y.shape[1])
    cost += regularization

    if index % 100 == 99:
        # print AL
        predicted = np.floor(2 * AL[0])
        training_accuracy = 100 * (len(predicted) - np.sum(np.logical_xor(predicted, train_y[0]))) / len(predicted)
        print("\nTraining Accuracy = " + str(training_accuracy) + " %   (iteration " + str(index + 1) + ")")
        print("Cost = " + str(cost))

    return cost


# propogate backward through all layers, calculating derivative at each step
def propogate_backward(train_y, cache, params):
    gradients, dZ = {}, {}

    # propogate backwards and chain rule derivativs together
    for i in range(len(hidden_layers), 0, -1):
        # first step in back propogation, is derivative of our loss function
        if i == len(hidden_layers):
            dZ["dZ"+str(i)] = cache["A"+str(i)] - train_y
        # otherwise, derivative of the activation function
        else:
            dZ["dZ"+str(i)] = np.multiply(cache["dA"+str(i)], np.dot(params["W"+str(i+1)].T, dZ["dZ"+str(i+1)]) )#* cache["D"+str(i)] / drop_prob)

        # add regularization factor lambd
        gradients["dW"+str(i)] = np.dot(dZ["dZ"+str(i)], cache["A"+str(i-1)].T) * (1. / train_y.shape[1]) + (regular_rate * params["W"+str(i)] * (1. / train_y.shape[1]))
        gradients["db"+str(i)] = np.sum(dZ["dZ"+str(i)], axis=1, keepdims=True) * (1. / train_y.shape[1])

    return gradients


# update params based on learning rate, current state, and backpropogation
def update_params(params, gradients):
    # iterate through and update all w, b params
    for i in range(len(hidden_layers)):
        # momentum factor first
        params["vW" + str(i+1)] = momentum * params["vW" + str(i+1)] + (1 - momentum) * gradients["dW" + str(i+1)]
        params["vb" + str(i+1)] = momentum * params["vb" + str(i+1)] + (1 - momentum) * gradients["db" + str(i+1)]

        # finally, update w and b params
        params["W"+str(i+1)] = params["W"+str(i+1)] - learning_rate * params["vW" + str(i+1)]
        params["b"+str(i+1)] = params["b"+str(i+1)] - learning_rate * params["vb" + str(i+1)]
   
    return params


# predict test data
def prediction(test_x, params):
    # Computes probabilities using forward propagation, and classifies to 0/1 using 0.5 as the threshold.
    predictions = propogate_forward(test_x, params)
    predictions = predictions["A"+str(len(hidden_layers))]

    return predictions


# print all test images. show image and print prediction to console
def print_predictions(test_x, test_y, predictions, classes):
    # get actual and prediction integer values
    actual = test_y[0]
    # round to neareast whole number (0 or 1)
    prediction = np.floor(2 * predictions[0])

    # print prediction to console and display the image
    for index in range(len(actual)):
        print ("\ny = " + str(actual[index]) + ", pred y = " +  "{:.8f}".format(predictions[0][index]))
        if (actual[index] == prediction[index]):
            print("Correct prediction! Image is a " + classes[np.squeeze(test_y[:, index])])
        else:
            print("Incorrect prediction. Image is a " + classes[np.squeeze(test_y[:, index])])

        plt.imshow(test_x[index])
        plt.show()

    training_accuracy = 100 * (len(prediction) - np.sum(np.logical_xor(prediction, actual))) / len(prediction)
    print("\nPrediction Accuracy = " + str(training_accuracy) + "%")



start()
