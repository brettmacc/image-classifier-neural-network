'''
    Brett Mac: May 2019

    This script converts a series of images into a single h5 dataset file.
    Image classification is determined by folder structure/naming in the directory.

'''

import numpy as np
import h5py
import json
import glob
import cv2
import random
import os

# choose image resolution (img_size x img_size), and percentage of dataset as train (vs test)
image_size = 220
single_size = 0.8
multi_size = 0.0
#save_file = "multi_test_325p.h5"
save_file = "all_images_220p.h5"


# this is the main script
def create_dataset():
    # we can grab all image paths/labels right away, then decide how to divide/use the info
    single_paths, single_labels, single_index, classes = get_single_images()
    multi_paths, multi_labels, multi_index = get_multi_images(classes)
    single_data = get_localization_data()

    # split single and multi image data into training / testing data
    single_images = read_image_data(single_paths)
    s_train_images, s_test_images, s_train_labels, s_test_labels = split_data(single_images, single_labels, single_index, classes, single_size)
    # "data" files are for localization 
    _, _, train_data, test_data = split_data(single_images, single_data, single_index, classes, single_size)

    multi_images = read_image_data(multi_paths)
    m_train_images, m_test_images, m_train_labels, m_test_labels = split_data(multi_images, multi_labels, multi_index, classes, multi_size)

    # now merge data to the easiest possible way to access
    train_images = np.concatenate((s_train_images, m_train_images), axis=0)
    train_labels = np.concatenate((s_train_labels, m_train_labels), axis=0)
    test_images  = np.concatenate((s_test_images, m_test_images), axis=0)
    test_labels  = np.concatenate((s_test_labels, m_test_labels), axis=0)

    # now just return and save all data into an h5 file
    return train_images, test_images, train_labels, test_labels, train_data, test_data, classes
    

# reads images folder, then creates class structure and parses images/data
def get_single_images():
    # get all the image classes based off of single-image folder names
    images_folder = './images/single/*'
    classes = glob.glob(images_folder)
    image_index = [0] * (len(classes) + 1)
    image_paths, image_labels = [], np.zeros((0, len(classes)))
    print("\nFound " + str(len(classes)) + " classes based on folder structure")

    # iterate through each class folder, and grab all images/image data
    for i in range(len(classes)):
        # grab every image for a certain "class folder", and give classes its user display name
        class_paths = glob.glob(classes[i] + '/*.png')
        classes[i] = classes[i][len(images_folder)-1:]

        # for this class, add every image path/label/data to the total sets
        image_paths += class_paths
        class_labels = np.zeros((len(class_paths), len(classes)))
        image_index[i+1] = len(image_paths)

        # we can automatically label the image based on the index of our folder
        for j in range(len(class_paths)):
            class_labels[j][i] = 1
        image_labels = np.concatenate((image_labels, class_labels), axis=0)

        print("Found " + str(len(class_paths)) + " images: for class " + classes[i])

    return image_paths, image_labels, image_index, classes


# reads images folder, and parses images/data into existing class structure
def get_multi_images(classes):
    # okay all single image files done at this point. multi is a little different
    images_folder = './images/multi/*'
    multi_classes = glob.glob(images_folder)
    image_paths, image_labels = [], np.zeros((0, len(classes)))
    image_index = [0] * (len(multi_classes) + 1)
    print("\nFound " + str(len(multi_classes)) + " multi-classes based on folder structure")

    # iterate through multi class folders, we need to define more complex labels based on folder names
    for i in range(len(multi_classes)):
        # grab all image paths, and determine which existing classes this multi-class is made of
        class_paths = glob.glob(multi_classes[i] + '/*.png')
        multi_classes[i] = multi_classes[i][len(images_folder)-1:].split("_")

        # for this multi-class, add every image path/label/data to the total sets
        image_paths += class_paths
        class_labels = np.zeros((len(class_paths), len(classes)))
        image_index[i+1] = len(image_paths)

        # for each image in this class, give a multi label based off of matching folder names
        for j in range(len(class_paths)):
            # label each class in the given multi-class
            for k in range(len(multi_classes[i])):
                class_labels[j][classes.index(multi_classes[i][k])] = 1
        image_labels = np.concatenate((image_labels, class_labels), axis=0)

        print("Found " + str(len(class_paths)) + " images: for class " + str(multi_classes[i]))

    return image_paths, image_labels, image_index


# we can add localization data to the dataset for augmentation purposes
def get_localization_data():
    # theres 4 different data points for every single label image (centerx, centery, width, height)
    data_points = ['bx', 'by', 'bw', 'bh']
    images_folder = './images/labeled_images/*'
    classes = glob.glob(images_folder)
    image_data = np.zeros((0, len(data_points)))
    print("\nFound " + str(len(classes)) + " single-classes with localization data, based on folder structure")

    # iterate through each image data folder and read all json files
    for i in range(len(classes)):
        # we dont need any image paths so this is easier, just grab the data directly
        class_paths = glob.glob(classes[i] + '/*.json')
        class_data = np.zeros((len(class_paths), len(data_points)))

        # read all json data and organize to same shape as image paths/labels
        for j in range(len(class_paths)):
            json_file = open(class_paths[j], 'r')
            img_json = json.loads(json_file.read())
            json_file.close()

            # read json file and just copy values directly into a new array
            for k in range(len(data_points)):
                class_data[j][k] = img_json[data_points[k]]
        image_data = np.concatenate((image_data, class_data), axis=0)

    return image_data


# given an array of image paths, actually read/load the image data
def read_image_data(images):
    # resolution of image is specified by user
    all_images = np.zeros((len(images), image_size, image_size, 3))
    print("\nReading " + str(len(images)) + " images from image folder...")

    # use cv2 to read image data
    for i in range(len(images)):
        # read an image and resize. cv2 load images as BGR, convert it to RGB
        image = cv2.imread(images[i])
        image = cv2.resize(image, (image_size, image_size), interpolation=cv2.INTER_CUBIC)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        all_images[i] = image

        if i % 500 == 0:
            print("Read " + str(i) + "/" + str(len(images)))

    return all_images


# split data into training and testing data, to make it easier in the CNN
def split_data(images, image_labels, class_index, classes, train_size):
    # quickly calculate total training size so we can initialize an array for speed
    total_train = 0
    for i in range(len(class_index) - 1):
        total_train += int(train_size*len(images[class_index[i]:class_index[i+1]]))
    
    # define arrays based off of "effective percentage" training data. 
    train_images = np.zeros((total_train, image_size, image_size, 3)) 
    test_images  = np.zeros((len(images) - total_train, image_size, image_size, 3))
    train_labels = np.zeros((total_train, image_labels.shape[1]))
    test_labels  = np.zeros((len(images) - total_train, image_labels.shape[1]))
    print("\nSplitting data into train / test data...")

    # divide the single images into train / test
    last_train, last_test = 0, 0
    for i in range(len(class_index) - 1):
        # use the class_index to select a certain class
        class_images = images[class_index[i]:class_index[i+1]]
        class_labels = image_labels[class_index[i]:class_index[i+1]]

        # split percentage based on user input size for training data percentage
        split = int(train_size*len(class_images))
        class_train_images = class_images[:split]
        class_test_images  = class_images[split:]
        class_train_labels = class_labels[:split]
        class_test_labels  = class_labels[split:]

        # add to our total split array
        start_train = last_train
        end_train   = start_train + class_train_images.shape[0]
        last_train  = end_train
        start_test  = last_test
        end_test    = start_test + class_test_labels.shape[0]
        last_test   = end_test

        train_images[start_train:end_train] = class_train_images
        train_labels[start_train:end_train] = class_train_labels
        test_images[start_test:end_test] = class_test_images
        test_labels[start_test:end_test] = class_test_labels

        print("Split class " + str(classes[i]))
        #print("start_train: " + str(start_train) + ", end_train: " + str(end_train))
        #print("start_test: " + str(start_test) + ", end_test: " + str(end_test))

    return train_images, test_images, train_labels, test_labels


# saves all of our data arrays into a single h5 file
def save_h5_file():
    # make sure datasets folder exists first
    directory = "./datasets/"
    if not os.path.exists(directory):
        os.makedirs(directory)

    # get all of our data by calling this
    train_images, test_images, train_labels, test_labels, train_data, test_data, classes = create_dataset()

    # write dataset to this file
    h5_file = h5py.File(directory + save_file, 'w')

    # in h5 you define a file first, then add the data
    h5_file.create_dataset("train_images", train_images.shape, np.uint8)
    h5_file.create_dataset("test_images", test_images.shape, np.uint8)
    h5_file.create_dataset("train_labels", train_labels.shape, np.uint8)
    h5_file.create_dataset("test_labels", test_labels.shape, np.uint8)
    h5_file.create_dataset("train_data", train_data.shape, np.uint8)
    h5_file.create_dataset("test_data", test_data.shape, np.uint8)
    h5_file.create_dataset("classes", (len(classes),), dtype=h5py.special_dtype(vlen=str)) # special encoding for strings

    # add all the image data
    h5_file["train_images"][...] = train_images
    h5_file["test_images"][...] = test_images
    h5_file["train_labels"][...] = train_labels
    h5_file["test_labels"][...] = test_labels
    h5_file["train_data"][...] = train_data
    h5_file["test_data"][...] = test_data
    h5_file["classes"][...] = classes
    h5_file.close()


#main script
save_h5_file()



