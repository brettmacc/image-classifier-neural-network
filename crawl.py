'''
    Brett Mac: May 2019

    This script crawls google/bing and saves a folder of images.
    Used to create a dataset to train an image classifier.

'''

from requests import get
from requests.exceptions import RequestException
from contextlib import closing
import urllib.request 
import sys
import os
import ssl


# max 800 images per search
ssl._create_default_https_context = ssl._create_unverified_context
img_limit = 800
max_attempts = 100
bing_thresh = 20

# take user input as search parameter for google images
engine = "google"
if len(sys.argv) > 1 and sys.argv[1] == "bing":
    engine = "bing"
print("\n")
search = input(engine + " image search: ")
print("\n")
max_images = int(input("max number of images: "))


# bing images limit to 28... random ass number but whatever
def bing_images(url, num_images):
    all_images = []
    limit = 28

    # cycle for requested num of images (this will auto cancel if requsting to many)
    for i in range(int(num_images/limit)):
        # check if searching over max image limit
        if i * limit > img_limit:
            return all_images

        page = bing_rec(url, i, 0)
        all_images.extend(page)
    
    #print(all_images)
    return all_images

def bing_rec(url, i, att):
    all_images = []
    if att > max_attempts:
        return all_images
    if att > 0:
        print("Ass Blasting Bing: Page " + str(i) + ", Attempt " + str(att+1))

    # request google images search. this just gives raw html text
    url_act = url + str(i*28) + "&form=hdrsc2"
    #print(url_act)
    raw_html = str(request_url(url_act))

    # grab encrypted image urls they all have src id
    all_urls = raw_html.split('href')

    # each iteration should give 20 urls in contained in all_urls
    for j in range(len(all_urls)):
        image = all_urls[j].split('"')

        # theres a way wider variety of images in bing 
        if len(image) > 1:
            if "http" in image[1]:
                # check for all image types
                if ".jpg" in image[1] or ".jpeg" in image[1] or ".png" in image[1]:
                    # check for broken file names and add a quick fix
                    if image[1].endswith(".jpg") or image[1].endswith(".jpeg") or image[1].endswith(".png"):
                        all_images.append(image[1])
                    else:
                        all_images.append(image[1].split(".jpg")[0] + ".jpg")

    if len(all_images) > bing_thresh:
        return all_images
    else:
        return bing_rec(url, i, att + 1)


# google limits search results to only 20...we have to modify search url and run many requests
def google_images(url, num_images):
    all_images = []
    limit = 20

    # cycle for requested num of images (this will auto cancel if requsting to many)
    for i in range(int(num_images/limit)):
        # request google images search. this just gives raw html text
        raw_html = str(request_url(url + str(i*limit)))

        # rip through all the bull shit html just get to image table
        images_table = raw_html.split("images_table")

        # when there is an empty images table the html split has only 3 params
        if len(images_table) < 4 or i * limit > img_limit:
            return all_images

        # grab encrypted image urls they all have src id
        all_urls = images_table[3].split("src")

        # each iteration should give 20 urls in contained in all_urls
        for j in range(len(all_urls)):
            image = all_urls[j].split('"')
            if "https" in image[1]:
                all_images.append(image[1])

    return all_images


# retrive all images using url and save them in created folder
def save_images(all_images):
    # create folder for the images to go in
    directory = "./images/" + search
    duplicate_num = ""
    if not os.path.exists(directory):
        os.makedirs(directory)
    else:
        name_found = 1
        # we'll check names first, don't want to overwrite/combine incorrect folders
        while name_found > 0:
            duplicate_num = " (" + str(name_found) + ")"
            if not os.path.exists(directory + duplicate_num):
                os.makedirs(directory + duplicate_num)
                name_found = -1
            name_found += 1

    # cycle through all the encrypted urls and save unique name to new unique folder
    for i in range(len(all_images)):
        # bing has different extensions. have to save the right type
        if ".jpg" in all_images[i] or ".jpeg" in all_images[i] or ".png" in all_images[i]: # bing
            ext = all_images[i].split(".") # get file extension
            ext = ext[len(ext) - 1]
        else:                                                               # google
            ext = "jpg"

        # finally, request and save image
        #print(all_images[i])
        request_image(all_images[i], directory + duplicate_num + "/" + search + duplicate_num + str(i+1) + "." + ext)


# try/catch for image request. also saves file locally at directory name
def request_image(url, name):
    try:
        with urllib.request.urlretrieve(url, name) as res:
            return
    except:
        return


# try/catch for url request. returns raw html content as a string
def request_url(url):
    try:
        with closing(get(url, stream=True)) as res:
            return res.content

    except RequestException as error:
        print(error)
        return None



# search bing or google?? file size is dramatically different so don't take both
if len(sys.argv) == 1 or sys.argv[1] == "google":
    google_images_url = "https://www.google.co.in/search?q=" + search + "&source=lnms&tbm=isch&start="
    all_images = google_images(google_images_url, max_images)
else:
    bing_images_url = "https://www.bing.com/images/search?q=" + search + "&first=" # + "&form=hdrsc2"
    all_images = bing_images(bing_images_url, max_images)


print("\nNumber of results: " + str(len(all_images)))
save_images(all_images)
