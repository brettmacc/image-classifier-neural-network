'''
    Brett Mac: May 2019

	Tensorflow deep neural network for image classification.
	Most effective if this is run through google colab GPU.

'''

import numpy as np
import h5py
import tensorflow as tf
from tensorflow.python.saved_model import tag_constants
import matplotlib.pyplot as plt
from PIL import Image


# save/load model. can set save to default to load and test another model
data_file = "single_100p.h5"
save_model = "single_class_model.ckpt"
load_model = "single_class_model.ckpt"


# Hyper-parameters for the network. Can configure everything here
num_iterations = 1
learning_rate = 0.0025
hidden_layers = [50, 25, 10, 23]
#regular_rate = 0.7
#drop_prob = 0.90
#momentum = 0.9
#activation_layers = ["relu", "relu", "relu", "sig"]


# This is the main function and starts the neural network
def start():
    # first step, get all image data
    train_x_orig, train_y, test_x_orig, test_y, classes = load_dataset()
    hidden_layers[len(hidden_layers)-1] = len(classes) # autoset last layer to correct dimension
    
    # reshape image data into a [Nx x m] matrix and normalize matrix
    train_x = (train_x_orig.reshape(train_x_orig.shape[0], -1).T) / 255.
    test_x = (test_x_orig.reshape(test_x_orig.shape[0], -1).T) / 255.

    # create machine learning model
    train_data = {"train_x": train_x, "train_y": train_y, "test_x": test_x, "test_y": test_y}
    params = nn_model(train_data)

    # get new predictions from trained model. test_x_orig needed to display images
    test_data = {"test_x": test_x, "test_y": test_y, "test_x_orig": test_x_orig}
    prediction(test_data, params, classes)


# This loads the h5 dataset file (in the program directory)
def load_dataset():
    # train/test data files are separated 
    train_dataset = h5py.File("./datasets/" + data_file, "r")

    # separate train/test data based on percentage
    train_x = np.array(train_dataset["train_images"][:])
    test_x  = np.array(train_dataset["test_images"][:])
    train_y = np.array(train_dataset["train_labels"][:]).T
    test_y  = np.array(train_dataset["test_labels"][:]).T

    # class names are available (from folder names in directory)
    classes = np.array(train_dataset["classes"][:])

    return train_x, train_y, test_x, test_y, classes


# create a machine learning nn model. initalize parameters and run optimization loop
def nn_model(train_data):
    # input and output placeholders
    X = tf.placeholder(tf.float32, shape = (train_data["train_x"].shape[0], None), name = "X")
    Y = tf.placeholder(tf.float32, shape = (train_data["train_y"].shape[0], None), name = "Y")

    # initialize w, b parameters based on input data, hidden layers, output nodes
    params = initialize_params(train_data["train_x"])

    # prop all examples forward through all layers
    cache = propogate_forward(X, params)

    # calculate loss for all examples and get cost function
    cost = cost_function(cache, Y)

    # backprop and update done with optimizer
    optimizer = tf.train.GradientDescentOptimizer(learning_rate = learning_rate).minimize(cost)
    init = tf.global_variables_initializer()
    saver = tf.train.Saver()

    # optimization loop
    with tf.Session() as sess:
        sess.run(init)
        saver.restore(sess, "./models/" + load_model)

        for i in range(num_iterations):
            _, c = sess.run([optimizer, cost], feed_dict={X: train_data["train_x"], Y: train_data["train_y"]})

            if ((i + 1) % 100 == 0) or (i == 0):
                print("\nIteration: " + str(i+1) + ",  Cost = " + str(c))
                # Calculate the correct predictions
                correct_prediction = tf.equal(tf.argmax(cache), tf.argmax(Y))
                accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
                print("Train Accuracy:", 100 * accuracy.eval({X: train_data["train_x"], Y: train_data["train_y"]}))
                print("Test Accuracy:", 100 * accuracy.eval({X: train_data["test_x"], Y: train_data["test_y"]}))

        # save model
        save_path = saver.save(sess, "./models/" + save_model)
        print("Model saved in path: %s" % save_path)

    return params


# initialize w and b vectors for all layers based on hidden_layers array and input data
def initialize_params(train_x):
    last_x, params = train_x.shape[0], {}

    # params object holds all w, b parameters (for all layers). W1 -> WL, b1 -> bL
    for i in range(len(hidden_layers)):
        params["W"+str(i+1)] = tf.get_variable("W"+str(i+1), [hidden_layers[i], last_x], initializer = tf.contrib.layers.xavier_initializer())
        params["b"+str(i+1)] = tf.get_variable("b"+str(i+1), [hidden_layers[i], 1], initializer = tf.zeros_initializer())
        last_x = hidden_layers[i]
        
        # add momentum parameters

    return params


# propogate forward through all layers. calculate linear combination and activation function
def propogate_forward(train_x, params):
    cache = {"A0": train_x}

    # cache holds all a, z params for linear/activation comination
    for i in range(len(hidden_layers)):
        cache["Z"+str(i+1)] = tf.add(tf.matmul(params["W"+str(i+1)], cache["A"+str(i)]), params["b"+str(i+1)])
        cache = activation(cache, i)
        
        # dropout regularization

    return cache["Z"+str(len(hidden_layers))]


# based on hyperparameters, set activation/deactivation function for each layer
def activation(cache, i):
    #if (activation_layers[i] == "relu"):
    cache["A"+str(i+1)] = tf.nn.relu(cache["Z"+str(i+1)])
    return cache


# calculate the average sum of the loss
def cost_function(cache, train_y):
    logits = tf.transpose(cache)
    labels = tf.transpose(train_y)
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits = logits, labels = labels))

    # compute regularization cost: regularization = lambd * regular_rate / (2 * train_y.shape[1])
    return cost


# predict test data
def prediction(test_data, params, classes):
    # shuffle test data
    permutation = list(np.random.permutation(test_data["test_x"].shape[1]))
    test_data["test_x"] = test_data["test_x"][:, permutation]
    test_data["test_y"] = test_data["test_y"][:, permutation]
    test_data["test_x_orig"] = test_data["test_x_orig"][permutation, :]

    # reprocess image 
    x = tf.placeholder(tf.float32, [100*100*3, test_data["test_x"].shape[1]])
    y = tf.placeholder(tf.float32, shape = (test_data["test_y"].shape[0], None), name = "Y")

    # propogate through params   
    z = propogate_forward(x, params)
    pred = tf.argmax(z)

    # load model
    saver = tf.train.Saver()
        
    # get predictions and calculate total test accuracy right away
    with tf.Session() as sess:
        saver.restore(sess, "./models/" + load_model)
        prediction = sess.run(pred, feed_dict = {x: test_data["test_x"], y: test_data["test_y"]})
        correct_prediction = tf.equal(tf.argmax(z), tf.argmax(y))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
        print("\nTest Accuracy = ", 100 * accuracy.eval({x: test_data["test_x"], y: test_data["test_y"]}))

    print(prediction.shape)
    print_predictions(prediction, classes, test_data)


# iterate through test data and analyze predictions
def print_predictions(prediction, classes, test_data):
    # loop through each test example. can view in/correct pictures for analysis
    for i in range(len(prediction)):
        print("\nPrediction: " + str(classes[np.squeeze(prediction[i])]))

        # print correct 
        if (test_data["test_y"][prediction[i]][i] == 1.):
            print("Correct! Image is: " + str(classes[prediction[i]]))
        # print incorrect
        else:
            for j in range(len(classes)):
                if (test_data["test_y"][j][i] == 1.):
                    print("Incorrect! Image is: " + str(classes[j]))

        plt.imshow(test_data["test_x_orig"][i])
        plt.show()


start()
