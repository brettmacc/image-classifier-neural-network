'''
    Brett Mac: May 2019

    This is an app to quickly label/localize images.
    Used to increase accuracy of a CNN by providing more specific data.

'''

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import h5py
import glob
import cv2
import random
import os
import json


'''
        CONTROLS:

        Use Mouse Click :   to select any 2 (opposite) corners of the object on the plot. the rectangle is drawn automatically

        Use Space Bar   :   to confirm a correctly drawn rectangle. you can only confirm when a rectangle has been drawn

        Use Escape Key  :   to undo drawing on current image.



        Also, use class_start/image_start to select a position in image folder to start labeling data
'''

class_start = 0
image_start = 0


# gather all images to process
def get_images():
    # Get classes based off of folder names
    img_folder = './images/single/*'
    class_addrs, classes = glob.glob(img_folder), glob.glob(img_folder)

    # Iterate through all classes, seperate test/training data/labels
    for i in range(len(class_addrs)):
        classes[i] = class_addrs[i][len(img_folder)-1:]
        class_addrs[i] = glob.glob(class_addrs[i] + '/*.png')

    return class_addrs, classes


# on mouse click draw points/rectangles on the image
def onclick(event):
    global ix, iy
    ix, iy = event.xdata, event.ydata
    print('x = %d, y = %d'%(ix, iy))

    # if rectangle is already drawn, don't allow any more points to be drawn
    if len(xcoords) % 2 == 0 and len(xcoords) != confirm[0]:
        return

    # add point drawn onto grid so user can see where they clicked
    plt.plot(ix, iy, 'ys')
    plt.draw()
    # keep track of all data
    xcoords.append(ix)
    ycoords.append(iy)

    # check if a pair of points have been added, then draw rectangle
    if len(xcoords) % 2 == 0 and len(xcoords) != 0:
        # make rectangle out of 2 points
        x1 = xcoords[len(xcoords) - 2]
        x2 = xcoords[len(xcoords) - 1]
        y1 = ycoords[len(ycoords) - 2]
        y2 = ycoords[len(ycoords) - 1]
        x_data = [x1, x1, x2, x2, x1]
        y_data = [y1, y2, y2, y1, y1]

        # draw each line of the rectangle
        for j in range(4):
            plt.plot(x_data[j:j+2], y_data[j:j+2], 'go-')

        plt.draw()

        # update drawn variable. this is for "undo" feature
        drawn.append(x1)
        drawn.append(x2)


# keyboard input: confirm label or redraw rectangle
def on_key(event):
    # press escape to reset the rectangle drawing for this image
    if event.key == "escape":
        # nothing drawn yet, so do nothing
        if len(xcoords) % 2 == 0 and len(xcoords) == confirm[0] and len(xcoords) == len(drawn):
            return
        # 2 points (full rectangle drawn)
        elif len(xcoords) % 2 == 0:
            del xcoords[-1]
            del ycoords[-1]
            del drawn[-1]
            del drawn[-1]

        # retry is global variable that pushes image loop back one iteration. there is no "undo" function so we redraw the image clean
        del xcoords[-1]
        del ycoords[-1]
        retry[0] = 1
        # close plot, which triggers next image
        plt.close()

    # if image is drawn, confirm
    elif event.key == " ":
        # confirm only if rectangle is drawn
        if len(xcoords) % 2 == 0 and len(xcoords) != confirm[0] and len(xcoords) == len(drawn):
            confirm[0] = len(drawn)
            # close plot, which triggers next image
            plt.close()
        else:
            print("Can only confirm image when rectangle is drawn")


# save image data for a single image. saves image with rectangle, as well as parameter data
def save_image(j, i):
    img = cv2.imread(img_addrs[j][i])
    img = cv2.resize(img, (100, 100), interpolation=cv2.INTER_CUBIC)

    # theres 2 coords for every rectangle
    x1 = int(xcoords[len(xcoords) - 2])
    x2 = int(xcoords[len(xcoords) - 1])
    y1 = int(ycoords[len(ycoords) - 2])
    y2 = int(ycoords[len(ycoords) - 1])

    # make sure we have coords in proper order
    if x1 > x2:
        x3 = x1
        x1 = x2
        x2 = x3
    if y1 > y2:
        y3 = y1
        y1 = y2
        y2 = y3

    # these are the crucial variables for machine learning. this is what its all about 
    height   = y2 - y1
    width    = x2 - x1
    center_x = (width / 2) + x1
    center_y = (height / 2) + y1

    # make sure new images folder exists. create it otherwise
    folder = "./images/labeled_images/" + str(classes[j]) + "/"
    if not os.path.exists(folder):
        os.makedirs(folder)

    # redraw rectangle and save cv image. this is for analysis purposes only
    img = cv2.rectangle(img, (x1,y1), (x2,y2), (0,255,0), 1)

    # match miti's name convention it has to be alphabetical
    image = folder + str(classes[j]) + "_"
    if i < 10:
        image = image + "00" + str(i)
    elif i < 100:
        image = image + "0" + str(i)
    else:
        image = image + str(i)

    # save image file and make a json of all the crucial data
    cv2.imwrite(image + '.png', img)
    save_data = {"bx": center_x, "by": center_y, "bw": width, "bh": height, "c": j}
    with open(image + '.json', 'w') as outfile:
        json.dump(save_data, outfile)


# get all the images and class data from the images folder
img_addrs, classes = get_images()
confirm, retry = [0], [0]
xcoords, ycoords, drawn = [], [], []


# make sure datasets folder exists first
directory = "./images/labeled_images/"
if not os.path.exists(directory):
    os.makedirs(directory)


# iterate through each class and all images. show user the image and wait for response
j = class_start
while j < len(img_addrs):
    # while loop cuz we redrawin shit baybe
    i = 0
    if j == class_start:
        i = image_start

    # iterate through each image i for a specific class j
    while i < len(img_addrs[j]):
        # create plot and open mouse/key listeners
        fig = plt.figure()
        cid = fig.canvas.mpl_connect('button_press_event', onclick)
        cid = fig.canvas.mpl_connect('key_press_event', on_key)

        # allow for easy/convenient save and load system
        # don't save if its the first image through the loop (might be redrawn still). save prev image only
        if j != class_start or i != image_start:
            # normally, grab previous image
            if i > 0:
                save_image(j, i - 1)
            # if its the first of the class, grab the last of the last class
            elif i == 0 and j != 0:
                save_image(j - 1, len(img_addrs[j-1]) - 1)

        # cv2 load images as BGR, convert it to RGB and plot it for user
        img = cv2.imread(img_addrs[j][i])
        img = cv2.resize(img, (100, 100), interpolation=cv2.INTER_CUBIC)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        plt.imshow(img)
        plt.show()

        # this code only runs after this image plot closes. so after a confirm/redraw decision is made
        # if redraw, retry is equal to 1 and i doesn't change. else i += 1
        i -= retry[0]
        retry[0] = 0

        i += 1

    j += 1

# and save the last image outside of loop i guess
save_image(len(img_addrs) - 1, len(img_addrs[len(img_addrs) - 1]) - 1)
